'use strict';

/**
 * @ngdoc function
 * @name thomasShowcaseApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the thomasShowcaseApp
 */
angular.module('thomasShowcaseApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
