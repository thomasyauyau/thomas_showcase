'use strict';

/**
 * @ngdoc function
 * @name thomasShowcaseApp.controller:ContactCtrl
 * @description
 * # ContactCtrl
 * Controller of the thomasShowcaseApp
 */
angular.module('thomasShowcaseApp')
  .controller('ContactCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
