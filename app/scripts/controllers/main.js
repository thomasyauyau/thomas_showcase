'use strict';

/**
 * @ngdoc function
 * @name thomasShowcaseApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the thomasShowcaseApp
 */
angular.module('thomasShowcaseApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
